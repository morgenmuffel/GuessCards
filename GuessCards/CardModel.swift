//
//  CardModel.swift
//  GuessCards
//
//  Created by Maksim on 17.02.2023.
//

import Foundation

struct CardModel: Hashable {
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(identifier)
    }
    
    static func ==(lhs: CardModel, rhs: CardModel) -> Bool {
        return lhs.identifier == rhs.identifier
    }
    
    var isFaceUp = false
    var isMatched = false
    private var identifier: Int
    
    private static var identifierNumber = 0
    private static func identifierGenerator() -> Int {
        identifierNumber += 1
        return identifierNumber
    }
 
    init() {
        self.identifier = CardModel.identifierGenerator()
    }
    
}
