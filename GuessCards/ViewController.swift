//
//  ViewController.swift
//  GuessCards
//
//  Created by Maksim on 15.02.2023.
//

import UIKit

class ViewController: UIViewController {
   
    lazy var game = GuessCardsGame(numberOfPairsOfCards: numberOfPairsOfCards)
    
    var numberOfPairsOfCards: Int {
        return (buttonCollection.count + 1) / 2
    }
    
    var touches = 0 {
        didSet {
            touchLabel.text = "Касаний: \(touches)"
        }
    }
    
     
    var emojiCollection = ["🐶","🐸","🐭","🙈","🐰","🐻","🐤","🐌","🐽","🦋","🐙","🦥","🐔"]
    
    var emojiDictionary = [CardModel:String]()
    
    func emojiIdentifier(for card: CardModel) -> String {
        if emojiDictionary[card] == nil {
            let randomIndex = Int(arc4random_uniform(UInt32(emojiCollection.count)))
            emojiDictionary[card] = emojiCollection.remove(at: randomIndex)
        }
        return emojiDictionary[card] ?? "?"
    }
    
    func updateViewFromModel() {
        for index in buttonCollection.indices {
            let button = buttonCollection[index]
            let card = game.cards[index]
            if card.isFaceUp {
                button.setTitle(emojiIdentifier(for: card), for: .normal)
                button.backgroundColor = .white
            } else {
                button.setTitle("", for: .normal)
                button.backgroundColor = card.isMatched ? .opaqueSeparator : .blue
            }
        }
    }
    

    @IBOutlet weak var touchLabel: UILabel!
    
    
    @IBAction func buttonAction(_ sender: UIButton) {
        
        touches += 1
        if let buttonIndex = buttonCollection.firstIndex(of: sender) {
            game.chooseCard(at: buttonIndex)
            updateViewFromModel()
        }
        
    }
    
    
    @IBOutlet var buttonCollection: [UIButton]!
    
    
    
    
    
    
    
    
    
}

